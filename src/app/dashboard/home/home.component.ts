import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ProjectCardModel} from '@app/shared/models/global.model';
import {HomeService} from '@app/shared/services/home.service';
import {Subscription} from 'rxjs/index';
import {MatPaginator} from '@angular/material';
import {catchError, debounceTime, distinctUntilChanged, isEmpty, tap} from 'rxjs/internal/operators';
import {fromEvent, of} from 'rxjs';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  private allSub: Subscription[] = [];
  public projectList: ProjectCardModel[] = [];
  public value: string;
  public totalCount: number;
  @ViewChild('search') search: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.value = '';
    this.loadProjectCards('1', '4', '');
  }

  ngAfterViewInit() {
    this.loadTotalCount();
    const search$ = fromEvent(this.search.nativeElement, 'keyup')
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadProjectParams();
        })
      ).subscribe();

    const pagination$ = this.paginator.page.pipe(
      tap(() => this.loadProjectParams())
    ).subscribe();

    this.allSub.push(pagination$, search$);
  }

  loadProjectCards(page: string, limit: string, query: string) {
    const getProject$ = this.homeService.getProjectCards(page, limit, query)
      .pipe(
        catchError(() => of([]))
      )
      .subscribe(result => {
        this.projectList = result;
      });

    this.allSub.push(getProject$);
  }

  loadProjectParams() {
    this.loadProjectCards(
      (this.paginator.pageIndex + 1).toString(),
      (this.paginator.pageSize).toString(),
      this.search.nativeElement.value
    );
  }

  loadTotalCount() {
    this.homeService.getProjectCards('', '', '').subscribe(res => {
      this.totalCount = res.length;
    });
  }

  ngOnDestroy() {
    for (const sub of this.allSub) {
      sub.unsubscribe();
    }
  }

}
