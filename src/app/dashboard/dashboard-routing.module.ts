import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./dashboard.component";

const routes: Routes = [
  {
    path: 'dashboard',
    redirectTo: 'dashboard/',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: '',
        loadChildren: '@app/dashboard/home/home.module#HomeModule'
      },
      {
        path: 'dashboard',
        redirectTo: 'dashboard/home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        loadChildren: '@app/dashboard/home/home.module#HomeModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'dashboard/home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
