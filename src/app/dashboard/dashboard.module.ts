import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {UiModule} from '../shared/ui/ui.module';

@NgModule({
  imports: [
    CommonModule,
    UiModule,
    DashboardRoutingModule,
  ],
  declarations: [
    DashboardComponent
  ]
})
export class DashboardModule {
  static forRoot(): ModuleWithProviders {
    return{
      ngModule: DashboardModule,
      providers: [  ]
    };
  }
}
