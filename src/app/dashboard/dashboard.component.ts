import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public sidenavWidth = 4;

  constructor() {}

  ngOnInit() {
  }

  expand() {
    this.sidenavWidth = 15;
  }

  collapse() {
    this.sidenavWidth = 4;
  }

}
