import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatCardModule, MatSidenavModule, MatDialogModule, MatPaginatorModule, MatTableModule, MatSortModule, MatButtonModule,
  MatFormFieldModule, MatInputModule, MatSelectModule, MatTabsModule, MatTreeModule, MatProgressBarModule,
  MatIconModule,
  MatDatepickerModule, MatNativeDateModule, MatListItem, MatListModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTreeModule,
    MatProgressBarModule,
    MatIconModule
  ],
  exports: [
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    MatTabsModule,
    MatTreeModule,
    MatProgressBarModule,
    MatIconModule
  ],
  declarations: []
})
export class MaterialModule { }
