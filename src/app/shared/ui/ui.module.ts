import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {MaterialModule} from "../material/material.module";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  declarations: [
    // Components
  ],
  exports: [
    RouterModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  providers: []
})
export class UiModule { }
