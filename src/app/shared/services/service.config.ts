export class ServiceConfig {
  static baseUrl = 'http://localhost:3000/';

  /**
   * ------------------------------------------------------------------
   * @description Home Endpoints
   * ------------------------------------------------------------------
   */

  // CRX Token Sale Statistics
  static project_cards: string = ServiceConfig.baseUrl + 'data';
}
