import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {ProjectCardModel} from '@app/shared/models/global.model';
import {ServiceConfig} from '@app/shared/services/service.config';
import {map} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private params = new HttpParams();

  constructor(private http: HttpClient) { }

  getProjectCards(page: string, limit: string, query: string): Observable<ProjectCardModel[]> {
    this.params = new HttpParams()
      .set('_page', page)
      .set('_limit', limit)
      .set('q', query);
    const url = ServiceConfig.project_cards;
    return this.http.get<ProjectCardModel[]>(url, {params: this.params}).pipe(
      map(res => {
        return res;
      })
    );
  }
}
