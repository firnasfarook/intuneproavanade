export interface ProjectCardModel {
  name: string;
  tags: {
    group: string;
    user: string
  };
  start: string;
  end: string;
  est_cost: string;
  predicted_cost: string;
}
